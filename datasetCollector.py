import numpy as np
import cv2
import argparse
import os

cap = cv2.VideoCapture(-1)


def take_background():
    frame_count = 0
    while frame_count is not 15:
        frame_count += 1
        ret, frame = cap.read()
        bgframe = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    print("Background taken")
    return bgframe


bgframe = take_background()
frame_cnt = 0

parser = argparse.ArgumentParser()
parser.add_argument('--outputDir', required=True)
parser.add_argument('--c',default=0,required=False)
args = parser.parse_args()

if not os.path.exists(args.outputDir):
    os.makedirs(args.outputDir)

while True:
    ret, frame = cap.read()
    if ret is False:
        break
    key = cv2.waitKey(1) & 0xFF
    frame_cnt += 1
    cv2.imshow("orignal frame", frame)
    if int(args.c):
        if key == ord('b'):
            bgframe = take_background()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        newimage = cv2.subtract(gray, bgframe, 0)
        blur = cv2.GaussianBlur(newimage, (3,3), 0)
        ret, threshhold = cv2.threshold(blur, 5, 255, cv2.THRESH_OTSU + cv2.THRESH_BINARY)
        kernel = np.ones((3, 3), np.uint8)
        morphed = cv2.morphologyEx(threshhold, cv2.MORPH_OPEN, kernel, iterations=3)
        transformed_image = cv2.bitwise_and(frame, frame, mask=morphed)
        cv2.imshow('frame', transformed_image)
        if key == ord('s'):
            cv2.imwrite(os.path.join(args.outputDir, 'image_{}.bmp'.format(frame_cnt)), transformed_image)
            print("image written")
    if key == ord('q'):
        cap.release()
        cv2.destroyAllWindows()
        break
exit()
